#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""preProcessor.py: Prepares the data in computation friendly format."""

from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score 
from optparse import OptionParser
import logging
import sys
from io import open
from os import path
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.utils import shuffle
import numpy

from util import NaiveBiasPredictor
import scipy
from scipy.sparse.coo import coo_matrix
from sklearn import cross_validation
import matplotlib.pyplot as plt

import pylab as pl
from sklearn.metrics import confusion_matrix

__author__ = "Vishwas B Sharma,Vinyas Maddi"
__email__ = "sharma.vishwas88@gmail.com,vinyas1987@gmail.com"

LOG_FORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

class Error(Exception):
  """Base error class for this module."""

class NoneValueException(Error):
  """Raised if the format passed by user is unknown."""

def main(options, args):
  #Fetch total data
  d = fetch_20newsgroups(subset='train')
  data = d.data
  voc = CountVectorizer().fit(data[:10000]).vocabulary_
  target = d.target

  labelled_data = data[:1000]
  labelled_targets = target[:1000]

  unlabelled_data_arr = [data[1000:2000],data[1000:3000],data[1000:4000],data[1000:5000],data[1000:6000],data[1000:7000],data[1000:8000],data[1000:9000],data[1000:10000]]
  
  test_data = data[10000:]
  test_targets = target[10000:]
  #d = fetch_20newsgroups(subset='test')
  #test_data = d.data
  #test_targets = d.target
  
  CV = CountVectorizer(vocabulary=voc)
  TF = TfidfTransformer()

  # All the necessary stuff :
  labelled_vectors = CV.fit_transform(labelled_data)
  test_vectors = CV.fit_transform(test_data)

  unlabelled_vectors = CV.fit_transform(unlabelled_data_arr[8])

  # Initial test with the model trained with labelled training data
  model = MultinomialNB(alpha=0.01).fit(TF.fit_transform(labelled_vectors), labelled_targets)
  print "Naive Bias Accuracy : "
  print metrics.accuracy_score(test_targets, model.predict(TF.fit_transform(test_vectors)))

  gamma = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,2,3,4,5]

  for g in gamma:

    uv = unlabelled_vectors.asfptype()
    uv.data *= g

    MAX_EM_ITERATIONS = 100
    iter_ctr = 1
    # EM Step to get better coef_ and class_log_prior_
    while iter_ctr < MAX_EM_ITERATIONS:
      #logging.info("Processing EM Iteration no %s" % iter_ctr)
      iter_ctr += 1

      # E step : 
      unlabelled_targets = model.predict(TF.fit_transform(uv))
      
      # M step : 
      prev_coef_ = model.coef_
      prev_class_log_prior_ = model.class_log_prior_

      total_targets = numpy.hstack((labelled_targets, unlabelled_targets))
      total_vectors = scipy.sparse.vstack([labelled_vectors, uv])
      model = MultinomialNB(alpha=0.01).fit(TF.fit_transform(total_vectors), total_targets)

      if (prev_coef_ == model.coef_).all() and (prev_class_log_prior_ == model.class_log_prior_).all():
        break;

    print "Weighted EM accuracy with gamma value " + str(g)
    print metrics.accuracy_score(test_targets, model.predict(TF.fit_transform(test_vectors)))
  
  #import pdb;pdb.set_trace()
  #Graphing
  '''xValues = range(1,10)
  plt.plot(xValues, nbAccuracy, color='#123456', label='NB Baseline Accuracy')
  plt.plot(xValues, emAccuracy, marker='x', color='r', label='EM Accuracy')

  plt.xlabel('Unlabelled Data in 1000s')
  plt.ylabel('Accuracy')
  plt.title('Accuracy with Unlabelled data variation')
  plt.legend()
  #Set limits properly
  #plt.xlim(0,12)
  #plt.ylim(ymin=0.68)
  plt.show()'''

  '''print "Confusion Matrix NB"
  print cmNB
  print "Confusion Matrix EM"
  print cmEM

  pl.matshow(cmNB)
  pl.title('Confusion matrix NB')
  pl.colorbar()
  pl.show()

  pl.matshow(cmEM)
  pl.title('Confusion matrix EM')
  pl.colorbar()
  pl.show()'''

def debug(type_, value, tb):
  if hasattr(sys, 'ps1') or not sys.stderr.isatty():
    # we are in interactive mode or we don't have a tty-like
    # device, so we call the default hook
    sys.__excepthook__(type_, value, tb)
  else:
    import traceback, pdb
    # we are NOT in interactive mode, print the exception...
    traceback.print_exception(type_, value, tb)
    print("\n")
    # ...then start the debugger in post-mortem mode.
    pdb.pm()

if __name__ == "__main__":
  parser = OptionParser()
  parser.add_option("--max_iter", dest="max_iter", help="Maximum number of EM iterations.")
  parser.add_option("--split_at", dest="split_at", help="Labelled data size in percent.")
  parser.add_option("--ngram_min", dest="ngram_min", help="Ngram minimum limit.")
  parser.add_option("--ngram_max", dest="ngram_max", help="Ngram maximum limit.")
  parser.add_option("--auto", dest="auto", action="store_true", default=False, help="Automatically generate and plot Accuracies for EM and Non EM.")
  parser.add_option("--diff", dest="diff", help="Difference in percentage of labelled data provided for training.")
  parser.add_option("-l", "--log", dest="log", help="log verbosity level",
                    default="INFO")
  (options, args) = parser.parse_args()
  if options.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, options.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOG_FORMAT)
  main(options, args)
