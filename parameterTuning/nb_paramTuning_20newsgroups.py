from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.grid_search import GridSearchCV
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
import numpy as np

training_set = fetch_20newsgroups()
trData = training_set.data
trTarget = training_set.target

lab_tr_data = trData[:1000]
lab_tr_tar = trTarget[:1000]

unlab_tr_data = trData[1001:]
unlab_tr_tar = trTarget[1001:]

#test_set = fetch_20newsgroups(subset='test')

count_vect = CountVectorizer()
tfidf_transformer = TfidfTransformer()

stopWords = ["a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be","because","been","but","by","can","cannot","could","dear","did","do","does","either","else","ever","every","for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","if","in","into","is","it","its","just","least","let","like","likely","may","me","might","most","must","my","neither","no","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she","should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet","you","your"]

X_train, X_test, Y_train, Y_test = cross_validation.train_test_split(lab_tr_data, lab_tr_tar, test_size=0.33, random_state=42)

text_clf = Pipeline([('vect', CountVectorizer()),('tfidf', TfidfTransformer()),('clf', MultinomialNB()),])
#parameters = {'vect__stop_words': [,stopWords],'vect__ngram_range': [(1, 1), (2, 2),(1,2),(3,3),(1,3)],'tfidf__use_idf': (True, False),'tfidf__norm': ('l1', 'l2'),'clf__alpha': (1, 0.1, 0.5, 1e-2, 0.03, 0.05, 1e-3, 1e-4),}
parameters = {'vect__stop_words': (None ,stopWords),'vect__ngram_range': [(1, 1), (2, 2)],'tfidf__use_idf': (True, False),'tfidf__norm': ('l1', 'l2'),'clf__alpha': (1, 0.1, 1e-2, 1e-3),}

gs_clf = GridSearchCV(text_clf,	 parameters)
gs_clf = gs_clf.fit(X_train, Y_train)

best_parameters, score, _ = max(gs_clf.grid_scores_, key=lambda x: x[1])
for param_name in sorted(parameters.keys()):
	print "%s: %r" % (param_name, best_parameters[param_name])

print "Best parameters set found on development set:"
print
print gs_clf.best_estimator_
print
print "Grid scores on development set:"
print
for params, mean_score, scores in gs_clf.grid_scores_:
    print "%0.3f (+/-%0.03f) for %r" % (
        mean_score, scores.std() / 2, params)
print
'''
print "Detailed classification report:"
print
print "The model is trained on the full development set."
print "The scores are computed on the full evaluation set."
print
y_true, y_pred = Y_test, gs_clf.predict(X_test)
print classification_report(y_true, y_pred)
print
cm = confusion_matrix(y_true, y_pred)

print cm

# Show confusion matrix
pl.matshow(cm)
pl.title('Confusion matrix')
pl.colorbar()
pl.show()
'''