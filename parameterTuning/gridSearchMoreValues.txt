clf__alpha: 0.01
tfidf__norm: 'l2'
tfidf__use_idf: True
vect__ngram_range: (1, 1)
Best parameters set found on development set:

Pipeline(clf=MultinomialNB(alpha=0.01, class_prior=None, fit_prior=True),
     clf__alpha=0.01, clf__class_prior=None, clf__fit_prior=True,
     tfidf=TfidfTransformer(norm=l2, smooth_idf=True, sublinear_tf=False, use_idf=True),
     tfidf__norm=l2, tfidf__smooth_idf=True, tfidf__sublinear_tf=False,
     tfidf__use_idf=True,
     vect=CountVectorizer(analyzer=word, binary=False, charset=utf-8,
        charset_error=strict, dtype=<type 'long'>, input=content,
        lowercase=True, max_df=1.0, max_features=None, max_n=None,
        min_df=2, min_n=None, ngram_range=(1, 1), preprocessor=None,
        stop_words=None, strip_accents=None, token_pattern=(?u)\b\w\w+\b,
        tokenizer=None, vocabulary=None),
     vect__analyzer=word, vect__binary=False, vect__charset=utf-8,
     vect__charset_error=strict, vect__dtype=<type 'long'>,
     vect__input=content, vect__lowercase=True, vect__max_df=1.0,
     vect__max_features=None, vect__max_n=None, vect__min_df=2,
     vect__min_n=None, vect__ngram_range=(1, 1), vect__preprocessor=None,
     vect__stop_words=None, vect__strip_accents=None,
     vect__token_pattern=(?u)\b\w\w+\b, vect__tokenizer=None,
     vect__vocabulary=None)

Grid scores on development set:

0.093 (+/-0.015) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.093 (+/-0.015) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.093 (+/-0.015) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.104 (+/-0.006) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.093 (+/-0.015) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.081 (+/-0.006) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.099 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.084 (+/-0.008) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.113 (+/-0.001) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.084 (+/-0.008) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.263 (+/-0.002) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.298 (+/-0.009) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.260 (+/-0.007) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.294 (+/-0.007) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.263 (+/-0.008) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.116 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.243 (+/-0.010) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.122 (+/-0.001) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.267 (+/-0.006) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.122 (+/-0.001) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.139 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.139 (+/-0.002) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.125 (+/-0.004) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.222 (+/-0.004) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.119 (+/-0.006) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.107 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.137 (+/-0.003) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.104 (+/-0.006) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.209 (+/-0.004) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.104 (+/-0.006) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.558 (+/-0.002) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.427 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.510 (+/-0.014) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.369 (+/-0.015) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.494 (+/-0.011) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.355 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.397 (+/-0.007) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.337 (+/-0.004) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.357 (+/-0.008) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.330 (+/-0.007) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.097 (+/-0.013) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.093 (+/-0.013) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.093 (+/-0.015) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.118 (+/-0.003) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.093 (+/-0.015) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.088 (+/-0.008) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.113 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.085 (+/-0.010) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.118 (+/-0.001) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.085 (+/-0.010) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.5}
0.355 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.351 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.334 (+/-0.002) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.342 (+/-0.008) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.337 (+/-0.004) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.160 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.303 (+/-0.007) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.175 (+/-0.002) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.312 (+/-0.007) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.179 (+/-0.004) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.5}
0.313 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.293 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.258 (+/-0.002) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.342 (+/-0.008) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.251 (+/-0.003) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.185 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.272 (+/-0.010) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.169 (+/-0.004) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.333 (+/-0.009) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.167 (+/-0.003) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.658 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.436 (+/-0.005) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.628 (+/-0.006) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.375 (+/-0.009) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.576 (+/-0.003) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.604 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.430 (+/-0.008) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.542 (+/-0.015) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.363 (+/-0.008) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.527 (+/-0.015) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.197 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.227 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.163 (+/-0.003) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.304 (+/-0.013) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.157 (+/-0.002) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.136 (+/-0.002) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.199 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.133 (+/-0.003) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.282 (+/-0.010) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.127 (+/-0.003) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.03}
0.640 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.440 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.590 (+/-0.005) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.378 (+/-0.011) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.555 (+/-0.014) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.507 (+/-0.008) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.421 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.469 (+/-0.007) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.370 (+/-0.006) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.454 (+/-0.008) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.03}
0.157 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.191 (+/-0.007) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.140 (+/-0.002) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.273 (+/-0.007) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.140 (+/-0.002) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.119 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.173 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.119 (+/-0.004) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.266 (+/-0.008) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.118 (+/-0.004) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.05}
0.615 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.436 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.557 (+/-0.009) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.376 (+/-0.014) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.527 (+/-0.015) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.449 (+/-0.014) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.406 (+/-0.008) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.424 (+/-0.012) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.363 (+/-0.007) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.415 (+/-0.010) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.05}
0.530 (+/-0.012) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.385 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.448 (+/-0.008) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.379 (+/-0.013) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.428 (+/-0.005) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.397 (+/-0.012) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.364 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.357 (+/-0.005) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.379 (+/-0.014) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.336 (+/-0.010) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.636 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.433 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.604 (+/-0.007) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.363 (+/-0.008) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.570 (+/-0.008) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.621 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.433 (+/-0.008) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.587 (+/-0.011) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.361 (+/-0.005) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.566 (+/-0.010) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.554 (+/-0.010) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.394 (+/-0.012) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.511 (+/-0.015) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.361 (+/-0.012) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.500 (+/-0.016) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.487 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.381 (+/-0.013) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.460 (+/-0.014) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.369 (+/-0.010) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.458 (+/-0.011) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l1', 'clf__alpha': 0.0001}
0.618 (+/-0.001) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.409 (+/-0.009) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.569 (+/-0.011) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.355 (+/-0.004) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.560 (+/-0.007) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': True, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.609 (+/-0.006) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.409 (+/-0.012) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.572 (+/-0.012) for {'vect__ngram_range': (1, 2), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.361 (+/-0.004) for {'vect__ngram_range': (3, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}
0.552 (+/-0.009) for {'vect__ngram_range': (1, 3), 'tfidf__use_idf': False, 'tfidf__norm': 'l2', 'clf__alpha': 0.0001}

