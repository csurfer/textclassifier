from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.grid_search import GridSearchCV
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
import pylab as pl
import numpy as np

training_set = fetch_20newsgroups()

trData = training_set.data
trTarget = training_set.target

training_set_lab = training_set.data[:1000]
training_set_target_lab = training_set.target[:1000]

test_set = fetch_20newsgroups(subset='test')

stopWords = ["a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be","because","been","but","by","can","cannot","could","dear","did","do","does","either","else","ever","every","for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","if","in","into","is","it","its","just","least","let","like","likely","may","me","might","most","must","my","neither","no","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she","should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet","you","your"]

text_clf = Pipeline([('vect', CountVectorizer(stop_words = stopWords)),('tfidf', TfidfTransformer()),('clf', MultinomialNB(alpha = 0.01)),])

text_clf.fit(training_set_lab, training_set_target_lab)
y_predicted = text_clf.predict(test_set.data)
print classification_report(test_set.target, y_predicted)


print
print
# Compute confusion matrix
cm = confusion_matrix(test_set.target, y_predicted)

#print cm

# Show confusion matrix
pl.matshow(cm)
pl.title('Confusion matrix')
pl.colorbar()
pl.show()