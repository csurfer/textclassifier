from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.grid_search import GridSearchCV
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
import numpy as np
import pylab as pl

training_set = fetch_20newsgroups()
trData = training_set.data
trTarget = training_set.target

lab_tr_data = trData[:1000]
lab_tr_tar = trTarget[:1000]

unlab_tr_data = trData[1001:]
unlab_tr_tar = trTarget[1001:]

#test_set = fetch_20newsgroups(subset='test')

count_vect = CountVectorizer()
tfidf_transformer = TfidfTransformer()

X_train, X_test, Y_train, Y_test = cross_validation.train_test_split(lab_tr_data, lab_tr_tar, test_size=0.33, random_state=42)

text_clf_ngram1_alpha001 = Pipeline([('vect', CountVectorizer(ngram_range = (1,1))),('tfidf', TfidfTransformer()),('clf', MultinomialNB(alpha = 0.01)),])
text_clf_ngram1_alpha01 = Pipeline([('vect', CountVectorizer(ngram_range = (1,1))),('tfidf', TfidfTransformer()),('clf', MultinomialNB(alpha = 0.1)),])
text_clf_ngram2_alpha001 = Pipeline([('vect', CountVectorizer(ngram_range = (2,2))),('tfidf', TfidfTransformer()),('clf', MultinomialNB(alpha = 0.01)),])
text_clf_ngram2_alpha01 = Pipeline([('vect', CountVectorizer(ngram_range = (2,2))),('tfidf', TfidfTransformer()),('clf', MultinomialNB(alpha = 0.1)),])

text_clf_ngram1_alpha001.fit(X_train, Y_train)
y_predicted_ngram1_alpha001 = text_clf_ngram1_alpha001.predict(X_test)

text_clf_ngram1_alpha01.fit(X_train, Y_train)
y_predicted_ngram1_alpha01 = text_clf_ngram1_alpha01.predict(X_test)

text_clf_ngram2_alpha001.fit(X_train, Y_train)
y_predicted_ngram2_alpha001 = text_clf_ngram2_alpha001.predict(X_test)

text_clf_ngram2_alpha01.fit(X_train, Y_train)
y_predicted_ngram2_alpha01 = text_clf_ngram2_alpha01.predict(X_test)

# Compute confusion matrix
cm_ngram1_alpha001 = confusion_matrix(Y_test, y_predicted_ngram1_alpha001)

cm_ngram1_alpha01 = confusion_matrix(Y_test, y_predicted_ngram1_alpha01)

cm_ngram2_alpha001 = confusion_matrix(Y_test, y_predicted_ngram2_alpha001)

cm_ngram2_alpha01 = confusion_matrix(Y_test, y_predicted_ngram2_alpha01)

#print cm

# Show confusion matrix
pl.matshow(cm_ngram1_alpha001)
pl.title('Confusion matrix - ngram = (1,1) & alpha = 0.001')
pl.colorbar()
pl.show()

# Show confusion matrix
pl.matshow(cm_ngram1_alpha01)
pl.title('Confusion matrix - ngram = (1,1) & alpha = 0.01')
pl.colorbar()
pl.show()

# Show confusion matrix
pl.matshow(cm_ngram2_alpha001)
pl.title('Confusion matrix - ngram = (2,2) & alpha = 0.001')
pl.colorbar()
pl.show()

# Show confusion matrix
pl.matshow(cm_ngram2_alpha01)
pl.title('Confusion matrix - ngram = (2,2) & alpha = 0.01')
pl.colorbar()
pl.show()
