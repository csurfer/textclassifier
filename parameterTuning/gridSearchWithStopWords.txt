clf__alpha: 0.01
tfidf__norm: 'l2'
tfidf__use_idf: True
vect__ngram_range: (1, 1)
vect__stop_words: ['a', 'able', 'about', 'across', 'after', 'all', 'almost', 'also', 'am', 'among', 'an', 'and', 'any', 'are', 'as', 'at', 'be', 'because', 'been', 'but', 'by', 'can', 'cannot', 'could', 'dear', 'did', 'do', 'does', 'either', 'else', 'ever', 'every', 'for', 'from', 'get', 'got', 'had', 'has', 'have', 'he', 'her', 'hers', 'him', 'his', 'how', 'however', 'i', 'if', 'in', 'into', 'is', 'it', 'its', 'just', 'least', 'let', 'like', 'likely', 'may', 'me', 'might', 'most', 'must', 'my', 'neither', 'no', 'nor', 'not', 'of', 'off', 'often', 'on', 'only', 'or', 'other', 'our', 'own', 'rather', 'said', 'say', 'says', 'she', 'should', 'since', 'so', 'some', 'than', 'that', 'the', 'their', 'them', 'then', 'there', 'these', 'they', 'this', 'tis', 'to', 'too', 'twas', 'us', 'wants', 'was', 'we', 'were', 'what', 'when', 'where', 'which', 'while', 'who', 'whom', 'why', 'will', 'with', 'would', 'yet', 'you', 'your']
Best parameters set found on development set:

Pipeline(clf=MultinomialNB(alpha=0.01, class_prior=None, fit_prior=True),
     clf__alpha=0.01, clf__class_prior=None, clf__fit_prior=True,
     tfidf=TfidfTransformer(norm=l2, smooth_idf=True, sublinear_tf=False, use_idf=True),
     tfidf__norm=l2, tfidf__smooth_idf=True, tfidf__sublinear_tf=False,
     tfidf__use_idf=True,
     vect=CountVectorizer(analyzer=word, binary=False, charset=utf-8,
        charset_error=strict, dtype=<type 'long'>, input=content,
        lowercase=True, max_df=1.0, max_features=None, max_n=None,
        min_df=2, min_n=None, ngram_range=(1, 1), preprocessor=None,
        stop_words=['a', 'able', ...],
        strip_accents=None, token_pattern=(?u)\b\w\w+\b, tokenizer=None,
        vocabulary=None),
     vect__analyzer=word, vect__binary=False, vect__charset=utf-8,
     vect__charset_error=strict, vect__dtype=<type 'long'>,
     vect__input=content, vect__lowercase=True, vect__max_df=1.0,
     vect__max_features=None, vect__max_n=None, vect__min_df=2,
     vect__min_n=None, vect__ngram_range=(1, 1), vect__preprocessor=None,
     vect__stop_words=['a', 'able', 'about', 'across', 'after', 'all', 'almost', 'also', 'am', 'among', 'an', 'and', 'any', 'are', 'as', 'at', 'be', 'because', 'been', 'but', 'by', 'can', 'cannot', 'could', 'dear', 'did', 'do', 'does', 'either', 'else', 'ever', 'every', 'for', 'from', 'get', 'got', 'had'...en', 'where', 'which', 'while', 'who', 'whom', 'why', 'will', 'with', 'would', 'yet', 'you', 'your'],
     vect__strip_accents=None, vect__token_pattern=(?u)\b\w\w+\b,
     vect__tokenizer=None, vect__vocabulary=None)

Grid scores on development set:

0.093 (+/-0.015) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.096 (+/-0.014) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.093 (+/-0.015) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.106 (+/-0.003) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.081 (+/-0.006) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.110 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.099 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.118 (+/-0.001) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 1}
0.263 (+/-0.002) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.454 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.298 (+/-0.009) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.316 (+/-0.003) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.116 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.358 (+/-0.006) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.243 (+/-0.010) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.291 (+/-0.009) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 1}
0.139 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.152 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.139 (+/-0.002) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.270 (+/-0.005) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.107 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.140 (+/-0.002) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.137 (+/-0.003) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.242 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.1}
0.558 (+/-0.002) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.637 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.427 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.355 (+/-0.001) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.355 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.570 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.397 (+/-0.007) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.351 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.1}
0.313 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.421 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.293 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.349 (+/-0.000) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.185 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.354 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.272 (+/-0.010) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.342 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.01}
0.658 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.661 (+/-0.002) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.436 (+/-0.005) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.373 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.604 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.648 (+/-0.008) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.430 (+/-0.008) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.367 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.01}
0.530 (+/-0.012) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.569 (+/-0.007) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.385 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.366 (+/-0.001) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.397 (+/-0.012) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.540 (+/-0.008) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.364 (+/-0.011) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.358 (+/-0.002) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l1', 'clf__alpha': 0.001}
0.636 (+/-0.005) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.651 (+/-0.003) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.433 (+/-0.006) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.376 (+/-0.000) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': True, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.621 (+/-0.004) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.627 (+/-0.006) for {'vect__ngram_range': (1, 1), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.433 (+/-0.008) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': None, 'tfidf__norm': 'l2', 'clf__alpha': 0.001}
0.375 (+/-0.004) for {'vect__ngram_range': (2, 2), 'tfidf__use_idf': False, 'vect__stop_words': [sssssss], 'tfidf__norm': 'l2', 'clf__alpha': 0.001}

