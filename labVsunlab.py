#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""preProcessor.py: Prepares the data in computation friendly format."""

from optparse import OptionParser
import logging
import sys
from io import open
from os import path
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.utils import shuffle
import numpy

from util import NaiveBiasPredictor
import scipy
from scipy.sparse.coo import coo_matrix
from sklearn import cross_validation
import matplotlib.pyplot as plt

import pylab as pl
from sklearn.metrics import confusion_matrix

__author__ = "Vishwas B Sharma,Vinyas Maddi"
__email__ = "sharma.vishwas88@gmail.com,vinyas1987@gmail.com"

LOG_FORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

class Error(Exception):
  """Base error class for this module."""

class NoneValueException(Error):
  """Raised if the format passed by user is unknown."""

def main(options, args):
  #Fetch total data
  d = fetch_20newsgroups(subset='train')
  data, test_data, target, test_targets = cross_validation.train_test_split(d.data, d.target, test_size=0.2, random_state=0)

  labelled_data = data[:1000]
  labelled_targets = target[:1000]

  unlabelled_data_arr = [data[1000:2000],data[1000:3000],data[1000:4000],data[1000:5000],data[1000:6000],data[1000:7000],data[1000:8000]]
  
  nbAccuracy = []
  emAccuracy = []

  for amt in range(7):
    em = []
    nb = []
    for ngram in range(1,6):
      extractor = CountVectorizer(ngram_range=(ngram, ngram))

      # All the necessary stuff :
      labelled_vectors = extractor.fit_transform(labelled_data)
      unlabelled_vectors = extractor.transform(unlabelled_data_arr[amt])
      test_vectors = extractor.transform(test_data)

      # Initial test with the model trained with labelled training data
      model = MultinomialNB().fit(labelled_vectors, labelled_targets)
      predicted = model.predict(test_vectors)
      nb.append(numpy.mean(predicted == test_targets))
      if amt == 6:
        cmNB = confusion_matrix(test_targets, predicted)

      MAX_EM_ITERATIONS = 100
      iter_ctr = 1
      # EM Step to get better coef_ and class_log_prior_
      while iter_ctr < MAX_EM_ITERATIONS:
        logging.info("Processing EM Iteration no %s" % iter_ctr)
        iter_ctr += 1

        # E step : 
        unlabelled_targets = model.predict(unlabelled_vectors)
        
        # M step : 
        prev_coef_ = model.coef_
        prev_class_log_prior_ = model.class_log_prior_

        total_targets = numpy.hstack((labelled_targets, unlabelled_targets))
        total_vectors = scipy.sparse.vstack([labelled_vectors, unlabelled_vectors])
        model = MultinomialNB().fit(total_vectors, total_targets)

        if (prev_coef_ == model.coef_).all() and (prev_class_log_prior_ == model.class_log_prior_).all():
          break;

      predicted = model.predict(test_vectors)
      em.append(numpy.mean(predicted == test_targets))
      if amt == 6:
        cmEM = confusion_matrix(test_targets, predicted)

    nbAccuracy.append(nb)
    emAccuracy.append(em)

  #Graphing
  xValues = range(1,6)
  plt.plot(xValues, nbAccuracy[0], marker='x', linestyle='--', color='#123456', label='NB vs ngrams')
  plt.plot(xValues, emAccuracy[0], marker='x', color='r', label='EM for 1000 Unlabelled')
  plt.plot(xValues, emAccuracy[1], marker='x', color='g', label='EM for 2000 Unlabelled')
  plt.plot(xValues, emAccuracy[2], marker='x', color='b', label='EM for 3000 Unlabelled')
  plt.plot(xValues, emAccuracy[3], marker='x', color='m', label='EM for 4000 Unlabelled')
  plt.plot(xValues, emAccuracy[4], marker='x', color='k', label='EM for 5000 Unlabelled')
  plt.plot(xValues, emAccuracy[5], marker='x', color='c', label='EM for 6000 Unlabelled')
  plt.plot(xValues, emAccuracy[6], marker='x', color='y', label='EM for 7000 Unlabelled')
  plt.xlabel('Ngrams')
  plt.ylabel('Accuracy')
  plt.title('Accuracy with Unlabelled data and ngram variation')
  plt.legend()
  #Set limits properly
  #plt.xlim(0,1)
  #plt.ylim(0,1.2)
  plt.show()

  print "Confusion Matrix NB"
  print cmNB
  print "Confusion Matrix EM"
  print cmEM

  pl.matshow(cmNB)
  pl.title('Confusion matrix NB')
  pl.colorbar()
  pl.show()

  pl.matshow(cmEM)
  pl.title('Confusion matrix EM')
  pl.colorbar()
  pl.show()


def debug(type_, value, tb):
  if hasattr(sys, 'ps1') or not sys.stderr.isatty():
    # we are in interactive mode or we don't have a tty-like
    # device, so we call the default hook
    sys.__excepthook__(type_, value, tb)
  else:
    import traceback, pdb
    # we are NOT in interactive mode, print the exception...
    traceback.print_exception(type_, value, tb)
    print("\n")
    # ...then start the debugger in post-mortem mode.
    pdb.pm()

if __name__ == "__main__":
  parser = OptionParser()
  parser.add_option("--max_iter", dest="max_iter", help="Maximum number of EM iterations.")
  parser.add_option("--split_at", dest="split_at", help="Labelled data size in percent.")
  parser.add_option("--ngram_min", dest="ngram_min", help="Ngram minimum limit.")
  parser.add_option("--ngram_max", dest="ngram_max", help="Ngram maximum limit.")
  parser.add_option("--auto", dest="auto", action="store_true", default=False, help="Automatically generate and plot Accuracies for EM and Non EM.")
  parser.add_option("--diff", dest="diff", help="Difference in percentage of labelled data provided for training.")
  parser.add_option("-l", "--log", dest="log", help="log verbosity level",
                    default="INFO")
  (options, args) = parser.parse_args()
  if options.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, options.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOG_FORMAT)
  main(options, args)
