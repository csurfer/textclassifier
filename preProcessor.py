#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""preProcessor.py: Prepares the data in computation friendly format."""

from optparse import OptionParser
import logging
import sys
from io import open
from os import path
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import numpy

from util import NaiveBiasPredictor
import scipy
from scipy.sparse.coo import coo_matrix
from sklearn import cross_validation
import matplotlib.pyplot as plt

__author__ = "Vishwas B Sharma,Vinyas Maddi"
__email__ = "sharma.vishwas88@gmail.com,vinyas1987@gmail.com"

LOG_FORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

class Error(Exception):
  """Base error class for this module."""

class NoneValueException(Error):
  """Raised if the format passed by user is unknown."""

def main(options, args):
  if options.max_iter is None:
    raise NoneValueException("Value of max_iter cannot be unknown.")
  elif options.auto is False and options.split_at is None:
    raise NoneValueException("Value of split_at cannot be unknown in non-auto mode.")
  elif options.auto is True and options.diff is None:
    raise NoneValueException("Value of diff cannot be unknown in auto mode.")

  # ngram values to be taken
  if options.ngram_min is not None and options.ngram_max is not None:
    ngram_min = int(options.ngram_min)
    ngram_max = int(options.ngram_max)
  else:
    ngram_min = 1
    ngram_max = 1

  # Getting the Data
  data = fetch_20newsgroups(subset='train')
  extractor = CountVectorizer(ngram_range=(ngram_min, ngram_max))
  vectors = extractor.fit_transform(data.data)

  # Splitting the data into train and test for cross-validation purposes because actual
  # test data should never be touched upon
  train_vectors, test_vectors, train_targets, test_targets = cross_validation.train_test_split(vectors, data.target, test_size=0.2, random_state=0)
  
  #Splitting the avalable training data set into labelled and unlabelled
  #parts for experimentation
  if options.auto:
    split_per = 1
    times = int(1/float(options.diff)) - 1
  else:
    split_per = 1 - float(options.split_at)
    times = 1

  xValues = []
  nbAccuracy = []
  emAccuracy = []

  for i in range(times):
    logging.info("The iteration of labelled data split is %s" % (i+1))
    if options.auto:
      split_per -= float(options.diff) 
      xValues.append(1-split_per)

    #Splitting of the vectors(training) according to the pre-determined percentage is done below.
    labelled_vectors, unlabelled_vectors, labelled_targets, u_targets = cross_validation.train_test_split(train_vectors, train_targets, test_size=split_per, random_state=0)
    # u_targets is assumed to be never existing for the purposes of experiment.

    # Initial test with the model trained with labelled training data
    model = MultinomialNB().fit(labelled_vectors, labelled_targets)
    predicted = model.predict(test_vectors)
    if options.auto:
      nbAccuracy.append(numpy.mean(predicted == test_targets))
    else:
      print "Initial Accuracy : %s" % numpy.mean(predicted == test_targets)

    MAX_EM_ITERATIONS = int(options.max_iter)
    iter_ctr = 1
    # EM Step to get better coef_ and class_log_prior_
    while iter_ctr < MAX_EM_ITERATIONS:
      logging.info("Processing EM Iteration no %s" % iter_ctr)
      iter_ctr += 1

      # E step : 
      unlabelled_targets = model.predict(unlabelled_vectors)
      
      # M step : 
      prev_coef_ = model.coef_
      prev_class_log_prior_ = model.class_log_prior_

      total_targets = numpy.hstack((labelled_targets, unlabelled_targets))
      total_vectors = scipy.sparse.vstack([labelled_vectors, unlabelled_vectors])
      model = MultinomialNB().fit(total_vectors, total_targets)

      if (prev_coef_ == model.coef_).all() and (prev_class_log_prior_ == model.class_log_prior_).all():
        break;

    predicted = model.predict(test_vectors)
    if options.auto:
      emAccuracy.append(numpy.mean(predicted == test_targets))
    else:
      print "Accuracy after EM : %s" % numpy.mean(predicted == test_targets)

  if options.auto:
    #Random Graphing
    plt.plot(xValues, nbAccuracy, marker='x', color='b',label='Without EM')
    plt.plot(xValues, emAccuracy, marker='o', linestyle='--', color='r', label='With EM')
    plt.xlabel('Labelled Data')
    plt.ylabel('Accuracy')
    plt.title('Accuracy with and without EM on Naive_Bayes')
    #plt.legend()
    #Set limits properly
    plt.xlim(0,1)
    plt.ylim(0,1.2)
    plt.show()

def debug(type_, value, tb):
  if hasattr(sys, 'ps1') or not sys.stderr.isatty():
    # we are in interactive mode or we don't have a tty-like
    # device, so we call the default hook
    sys.__excepthook__(type_, value, tb)
  else:
    import traceback, pdb
    # we are NOT in interactive mode, print the exception...
    traceback.print_exception(type_, value, tb)
    print("\n")
    # ...then start the debugger in post-mortem mode.
    pdb.pm()

if __name__ == "__main__":
  parser = OptionParser()
  parser.add_option("--max_iter", dest="max_iter", help="Maximum number of EM iterations.")
  parser.add_option("--split_at", dest="split_at", help="Labelled data size in percent.")
  parser.add_option("--ngram_min", dest="ngram_min", help="Ngram minimum limit.")
  parser.add_option("--ngram_max", dest="ngram_max", help="Ngram maximum limit.")
  parser.add_option("--auto", dest="auto", action="store_true", default=False, help="Automatically generate and plot Accuracies for EM and Non EM.")
  parser.add_option("--diff", dest="diff", help="Difference in percentage of labelled data provided for training.")
  parser.add_option("-l", "--log", dest="log", help="log verbosity level",
                    default="INFO")
  (options, args) = parser.parse_args()
  if options.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, options.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOG_FORMAT)
  main(options, args)
