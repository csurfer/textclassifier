from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.grid_search import GridSearchCV
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import numpy as np

training_set = fetch_20newsgroups()
trData = training_set.data
trTarget = training_set.target

training_set_lab = training_set.data[:1000]
training_set_target_lab = training_set.target[:1000]
training_set_unl = training_set.data[1001:]
training_set_target_unl = training_set.target[1001:]

# parameters
param_alpha = 0.01

text_clf = Pipeline([('vect', CountVectorizer()),('tfidf', TfidfTransformer()),('clf', MultinomialNB(alpha = param_alpha)),])

# Try predicting the test data with the MultinomialNB
predictions = text_clf.predict(test_set.data)
accuracy = np.mean(predictions == test_set.target)

training_set_lab = training_set.data[:1000]
training_set_target_lab = training_set.target[:1000]
training_set_unl = training_set.data[1001:]
training_set_target_unl = training_set.target[1001:]

print 'accuracy is' , accuracy*100

# before starting iterations
_ = clf_multiNB.fit(training_set_lab, training_set_target_lab)
predictions_unl = clf_multiNB.predict(training_set_unl)

training_set_merged = training_set_lab + training_set_unl
training_set_target_merged = np.hstack((training_set_target_lab, predictions_unl))

for x in range(0,10):
	_ = clf_multiNB.fit(training_set_merged, training_set_target_merged)
	#predictions = clf_multiNB.predict(training_set_unl)
	#accuracy = np.mean(predictions == training_set_target_unl)
	predictions_unl = clf_multiNB.predict(training_set_unl)
	predictions_test = clf_multiNB.predict(test_set.data)
	accuracy_unl = np.mean(predictions_unl == training_set_target_unl)
	accuracy_test = np.mean(predictions_test == test_set.target)
	print 'accuracy on unlabelled data in iteration is' , accuracy_unl*100
	print 'accuracy on test data in iteration is' , accuracy_test*100	
	training_set_target_merged = np.hstack((training_set_target_lab, predictions_unl))	

print 'accuracy is' , accuracy*100

print metrics.classification_report(test_set.target, predictions, target_names=test_set.target_names)
