#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""util.py: Utility functions."""

from optparse import OptionParser
import logging
import sys
from io import open
from os import path

import numpy

__author__ = "Vishwas B Sharma, Vinyas Maddi"
__email__ = "sharma.vishwas88@gmail.com, vinyas1987@gmail.com"

LOG_FORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

class NaiveBiasPredictor(object):

  def run(self, featureMatrix, priorMatrix, testDataMatrix):
    # Feature vector * Word probability given a class or
    # Sum of all Count Vector * Log of word probability given a class
    val = testDataMatrix * numpy.transpose(featureMatrix)
    res = val + priorMatrix
    return res.argmax(1)