from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.grid_search import GridSearchCV
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import numpy as np

training_set = fetch_20newsgroups()

trData = training_set.data
trTarget = training_set.target

training_set_lab = training_set.data[:1000]
training_set_target_lab = training_set.target[:1000]

test_set = fetch_20newsgroups(subset='test')

text_clf = Pipeline([('vect', CountVectorizer()),('tfidf', TfidfTransformer()),('clf', MultinomialNB(alpha = 0.01)),])

text_clf.fit(training_set_lab, training_set_target_lab)
y_predicted = text_clf.predict(test_set.data)
print classification_report(test_set.target, y_predicted)
